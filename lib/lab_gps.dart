import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';

class LabGps extends StatefulWidget {
  const LabGps({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LabGps();
  }
}

class _LabGps extends State<LabGps> {
  Future<Position> obtenerGps() async {
    //Verificar si la ubicación del dispositivo está habilitada
    bool bGpsHabilitado = await Geolocator.isLocationServiceEnabled();
    if (!bGpsHabilitado) {
      return Future.error('Por favor habilite el servicio de ubicación.');
    }
    //Validar permiso para utilizar los servicios de localización
    LocationPermission bGpsPermiso = await Geolocator.checkPermission();
    if (bGpsPermiso == LocationPermission.denied) {
      bGpsPermiso = await Geolocator.requestPermission();
      if (bGpsPermiso == LocationPermission.denied) {
        return Future.error('Se denegó el permiso para obtener la ubicación.');
      }
    }
    if (bGpsPermiso == LocationPermission.deniedForever) {
      return Future.error(
          'Se denegó el permiso para obtener la ubicación de forma permanente.');
    }
    //En este punto los permisos están habilitados y se puede consultar la ubicación
    return await Geolocator.getCurrentPosition();
  }

  Future<void> abrirUrl(final String sUrl) async {
    final Uri oUri = Uri.parse(sUrl);
    try {
      await launchUrl(
          oUri, //Ej: http://www.google.com/maps/place/6.2502089,-75.5706711
          mode: LaunchMode.externalApplication);
    } catch (oError) {
      return Future.error('No fue posible abrir la url: $sUrl.');
    }
  }

  goToLocation() async {
    Position x = await obtenerGps();
    String url =
        "http://www.google.com/maps/place/${x.latitude},${x.longitude}";
    await abrirUrl(url);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Geolocator Page'),
            backgroundColor: Colors.lightBlue,
            automaticallyImplyLeading: false,
          ),
          body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              child: Center(
                  child: ElevatedButton(
                      onPressed: () => {goToLocation()},
                      child: const Text('Go to your current Location!'))))),
    );
  }
}
